﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


public class GameController : MonoBehaviour
{
    public int whoTurn; // 0 = Serbia, 1 = Croatia
    public int whoTurnedFirst;
    public int turnCount; //counts the number of turn played
    public GameObject[] turnIcons; //displays whos turn
    public Sprite[] playIcons; //0 = Serbia, 1 = Croatia
    public Button[] tictactoeSpaces; //playable space for game
    public int[] markSpaces; //space was marked
    public Text winnerText;
    public GameObject[] winningLine;
    public GameObject winnerPanel;
    public int serbiaScore;
    public int croatiaScore;
    public int draw;
    public Text serbiaScoreText;
    public Text croatiaScoreText;
    public Text drawText;


    void Start()
    {
        GameSetup();       
    }

    void GameSetup()
    {
        whoTurnedFirst = whoTurn;
        turnCount = 0;
        turnIcons[whoTurn].SetActive(true);
        turnIcons[whoTurn == 0 ? 1 : 0].SetActive(false);
        for (int i = 0; i < tictactoeSpaces.Length; i ++)
        {
            tictactoeSpaces[i].interactable = true;
            tictactoeSpaces[i].GetComponent<Image>().sprite = null;
        }

        for(int i = 0; i < markSpaces.Length; i++)
        {
            markSpaces[i] = -100;
        }
    }

    
    void Update()
    {
        
    }

    public void TicTacToeButton(int WichNumber)
    {
        tictactoeSpaces[WichNumber].image.sprite = playIcons[whoTurn];
        tictactoeSpaces[WichNumber].interactable = false;

        markSpaces[WichNumber] = whoTurn+1;
        turnCount++;
        if (turnCount > 4)
        {
            if (WinnerCheck())
                return;
        }


        if (whoTurn == 0)
        {

            whoTurn = 1;
            turnIcons[0].SetActive(false);
            turnIcons[1].SetActive(true);
        }
        
        else
        {
            whoTurn = 0;
            turnIcons[0].SetActive(true);
            turnIcons[1].SetActive(false);
        }
    }

    bool WinnerCheck()
    {
        int solut1 = markSpaces[0] + markSpaces[1] + markSpaces[2];
        int solut2 = markSpaces[3] + markSpaces[4] + markSpaces[5];
        int solut3 = markSpaces[6] + markSpaces[7] + markSpaces[8];
        int solut4 = markSpaces[0] + markSpaces[3] + markSpaces[6];
        int solut5 = markSpaces[1] + markSpaces[4] + markSpaces[7];
        int solut6 = markSpaces[2] + markSpaces[5] + markSpaces[8];
        int solut7 = markSpaces[0] + markSpaces[4] + markSpaces[8];
        int solut8 = markSpaces[2] + markSpaces[4] + markSpaces[6];
        var solutions = new int[] { solut1, solut2, solut3, solut4, solut5, solut6, solut7, solut8 };
        for (int i = 0; i < solutions.Length; i++)
        {
            if (solutions[i] == 3 * (whoTurn + 1))
            {
                WinnerDisplay(i);
                return true;
                
            }
        }

        if (markSpaces.Count(x => x <= 0) <= 0)

        {
            WinnerDisplay(100);
            return true;
        }
        return false;
    }

    void WinnerDisplay(int IndexIn)
    {
        winnerPanel.gameObject.SetActive(true);

        if (IndexIn < 100)

        {
            if (whoTurn == 0)
            {
                serbiaScore++;
                serbiaScoreText.text = serbiaScore.ToString();
                winnerText.text = "Player Serbia Wins!";
                winningLine[IndexIn].SetActive(true);
                
            }

            else if (whoTurn == 1)
            {
                croatiaScore++;
                croatiaScoreText.text = croatiaScore.ToString();
                winnerText.text = "Player Croatia Wins!";
                winningLine[IndexIn].SetActive(true);
                

            }

            
        }

        else 
        {
            draw++;
            drawText.text = draw.ToString();
            winnerText.text = "Draw!!!";
            whoTurn = whoTurnedFirst == 0 ? 1 : 0;
        }
    
       }

    public void Rematch()
    {
        GameSetup();
        for (int i = 0; i < winningLine.Length; i++)
        {
            winningLine[i].SetActive(false);
        }       
        winnerPanel.SetActive(false);
        

    }

    public void Restart()
    {
        Rematch();
        serbiaScore = 0;
        croatiaScore = 0;
        serbiaScoreText.text = "0";
        croatiaScoreText.text = "0";
    }
}
